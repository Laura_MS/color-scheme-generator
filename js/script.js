var color = function(h, s, l, hsv, rgb, hex) {
    this.hex; //TODO: define
    this.h = h;
    this.s = s;
    this.l = l;
    this.hsv = hsv;
    this.rgb = rgb;
    this.hex = hex;
    color.prototype.toString = function() {
        return `${this.h},${this.s}%,${this.l}%`;
    }
}

var colorField = document.getElementsByName("hex")[0];
var errorString;

colorField.addEventListener("input", function(event) {
    document.querySelector(".error").style.opacity = 1;
    var bttn = document.getElementsByName("fetch")[0];
    bttn.disabled = true;

    if(colorField.validity.tooShort) { errorString = "Code too short."; }
    else if(colorField.validity.tooLong) { errorString = "Code too long."; } 
    else if(colorField.validity.patternMismatch) { errorString = "Invalid hex code."; }
    else { errorString = ""; bttn.disabled = false; }

    var i = 1;
    //typing too quickly creates multiple instances of i
    var fader = setInterval(function(){
        document.getElementsByClassName('error')[0].style.opacity = i;
        i-=0.1;
        if(i <= 0) {
            window.clearInterval(fader);
            i=1;
        }
    },100);
    
    colorField.setCustomValidity(errorString);
    document.getElementsByClassName("error")[0].innerText = errorString;
    document.getElementsByClassName("error")[0].style.display = "inline-block";

    //https://stackoverflow.com/questions/32401437
    document.addEventListener('invalid', (function () {
        return function (e) {
            e.preventDefault();
        };
    })(), true);
});

const hueClamp = function(value) {
    if(value < 0) { value += 360; }
    if(value > 360) { value -= 360; }

    return value;
}

const hueSpin = function(h, offset) {
    h += offset;
    h = hueClamp(h,0,360);
    return h;
}

const createScheme = function(h,s,l, hsv, rgb, hex, type) {
    var scheme = [];
    scheme.push(new color(h,s,l, hsv, rgb, hex));

    switch(type) {
        case "monochromatic": 
            if(l>80) {
                scheme.push(new color(h,s,l-20, hsv, rgb, hex));
                scheme.push(new color(h,s,l-40, hsv, rgb, hex));
            }
            else if(l<20) {
                scheme.push(new color(h,s,l+20, hsv, rgb, hex));
                scheme.push(new color(h,s,l+40, hsv, rgb, hex));
            }
            else {
                scheme.push(new color(h,s,l+20, hsv, rgb, hex));
                scheme.push(new color(h,s,l-20, hsv, rgb, hex));
            }
            break;
        case "analogous":
            scheme.push(new color(hueSpin(h,30),s,l, hsv, rgb, hex));
            scheme.push(new color(hueSpin(h,-30),s,l,hsv, rgb, hex));
            break;
        case "triadic":
            scheme.push(new color(hueSpin(h,120),s,l, hsv, rgb, hex));
            scheme.push(new color(hueSpin(h,-120),s,l, hsv, rgb, hex));
            break;
        case "tetradic":
            scheme.pop();
            scheme.push(new color(hueSpin(h,30),s,l, hsv, rgb, hex));
            scheme.push(new color(hueSpin(h,-30),s,l, hsv, rgb, hex));
            scheme.push(new color(hueSpin(h,150),s,l, hsv, rgb, hex));
            scheme.push(new color(hueSpin(h,-150),s,l, hsv, rgb, hex));
            break;
    }

    saveScheme(h,s,l,type,hsv,rgb, hex);
    return scheme;
}

const displayScheme = function(scheme) {
    document.querySelector("#output table tr").innerHTML = "";
    for(var i=0; i<scheme.length; i++) {
        let colorBlock = document.createElement("div");
        colorBlock.style.display = "inline-block";
        colorBlock.style.width = "100px";
        colorBlock.style.height = "100px";
        colorBlock.style.backgroundColor = `hsl(${scheme[i].h},${scheme[i].s}%, ${scheme[i].l}%)`;

        let hslBlock = document.createElement("div");
        let hslText = document.createTextNode(scheme[i].toString());
        hslBlock.appendChild(hslText);

        let hsvBlock = document.createElement("div");
        let hsvText = document.createTextNode(scheme[i].hsv);
        hsvBlock.appendChild(hsvText);

        let rgbBlock = document.createElement("div");
        let rgbText = document.createTextNode(scheme[i].rgb);
        rgbBlock.appendChild(rgbText);

        let td = document.createElement("td");
        td.appendChild(colorBlock);
        td.appendChild(hslBlock);
        td.appendChild(hsvBlock);
        td.appendChild(rgbBlock);

        document.querySelector("#output table tr").appendChild(td);
    }
}

const loadScheme = function() {
    if (Object.keys(localStorage).length > 0) {
        var ls = window.localStorage;
        var scheme = createScheme(parseInt(ls.getItem("h")), parseInt(ls.getItem("s")), parseInt(ls.getItem("l")), ls.getItem("type"), ls.getItem("hsv"), ls.getItem("rgb"), ls.getItem("hex"));
        displayScheme(scheme);
    }
}

window.onload = function() {
    loadScheme();
}

const saveScheme = function(h,s,l,type, hsv, rgb, hex) {
    window.localStorage.setItem("h", h);
    window.localStorage.setItem("s", s);
    window.localStorage.setItem("l", l);
    window.localStorage.setItem("type",type);
    window.localStorage.setItem("hsv",hsv);
    window.localStorage.setItem("rgb", rgb);
    window.localStorage.setItem("hex", hex);
}

document.getElementsByName("fetch")[0].addEventListener("click", function() {
    var i = 75;
    document.getElementsByName("fetch")[0].innerText = "Saving Scheme";
    var buttonAnim = setInterval(function(){
        document.getElementsByName("fetch")[0].style.backgroundColor = "hsl(211, 100%,"+i+"%)";
        if(--i===50) {
            window.clearInterval(buttonAnim);
            i=75;
            document.getElementsByName("fetch")[0].innerText = "Get some colors";
        }
    },100);
});

window.onsubmit = function(e) {
    e.preventDefault();

    fetch(`https://www.thecolorapi.com/id?hex=${document.getElementsByName("hex")[0].value}&format=json`)
    .then(function(response) {
        return response.json();
    })
    .then(function(c) {
        var hsv = `${c.hsv.h},${c.hsv.s},${c.hsv.v}`;
        var rgb = `${c.rgb.r},${c.rgb.g},${c.rgb.b}`;
        var hex = document.getElementsByName("hex")[0].value;
        var myColor = new color(c.hsl.h, c.hsl.s, c.hsl.l, hsv, rgb, hex);
        var type = document.querySelector('input[name="type"]:checked').value;
        displayScheme(createScheme(c.hsl.h, c.hsl.s, c.hsl.l, hsv, rgb, hex, type));
    });
}